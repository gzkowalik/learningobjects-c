#include "zwierze.h"
#include <iostream>
#include <string>
using namespace std;

int zwierze::ile_zwierzat = 0;
string zwierze::getName() const { return nazwa; }
int zwierze::getAge() const { return wiek; }
void zwierze::setName(string name) {
	nazwa = name;
}
void zwierze::setAge(int age) {
	wiek = age;
}
void zwierze::dajG這s() {
	cout << getName() << " daje glos. " << endl;
}
void zwierze::jedz() {
	cout << getName() << " je. " << endl;
}
ostream & operator<<(ostream &wyjscie, const zwierze &z) {
	return wyjscie << "Wiek: " << z.getAge() << ", imie: " << z.getName() << endl;
}

istream & operator>>(istream &wejscie, zwierze &z) {
	int age;
	cout << "Wpisz wiek zwierzecia: " << endl;
	wejscie >> age;
	z.setAge(age);
	string name;
	cout << "Wpisz imie zwierzecia: " << endl;
	wejscie >> name;
	z.setName(name);

	return wejscie;
}
zwierze::~zwierze() {
	cout << "Zwierze " << getName() << " zdechlo." << endl;
}
void zwierzeDomowe::przytul(zwierze *z) {
	cout << " lubi sie przytulac z " << z->getName() << endl;
}
void pies::dajG這s() {
	zwierze::dajG這s();
	cout << "Hau hau." << endl;
}
void pies::jedz() {
	zwierze::jedz();
	cout << "Karma zostala zjedzona." << endl;
}
void pies::przytul(zwierze* z) {
	cout << this->getName();
	zwierzeDomowe::przytul(z);
}
void kot::dajG這s() {
	zwierze::dajG這s();
	cout << "Miau." << endl;
}
void kot::jedz() {
	zwierze::jedz();
	cout << "Mleko zostalo wypite." << endl;
}
void kot::przytul(zwierze* z) {
	cout << this->getName();
	zwierzeDomowe::przytul(z);
}
void ptak::lataj() {
	cout << "Ja latam!" << endl;
}
void papuga::dajG這s() {
	zwierze::dajG這s();
	cout << "Ja gadam!" << endl;
}
void papuga::jedz() {
	zwierze::jedz();
	cout << "Owoce zostaly zjedzone. " << endl;
}
void wlasciciel::setImie(string name) {
	imie = name;
}
void wlasciciel::setKoty(int ile) {
	koty = ile;
}
void wlasciciel::setPapugi(int ile) {
	papugi = ile;
}
void wlasciciel::setPsy(int ile) {
	psy = ile;
}
void wlasciciel::ustaw() {
	gromada = new zwierze*[getKoty() + getPapugi() + getPsy()];
	for (int i = 0; i < getKoty(); i++)
		gromada[i] = new kot;
	for (int i = getKoty(); i < getKoty()+getPapugi(); i++)
		gromada[i] = new papuga;
	for (int i = getKoty() + getPapugi(); i < getKoty() + getPapugi() + getPsy(); i++)
		gromada[i] = new pies;
}
istream & operator>>(istream &wejscie, wlasciciel &w) {
	int ile;
	string name;
	cout << "Jak masz na imie? " << endl;
	wejscie >> name;
	w.setImie(name);
	cout << "Ile masz papug? " << endl;
	wejscie >> ile;
	w.setPapugi(ile);
	cout << "Ile masz kot闚? " << endl;
	wejscie >> ile;
	w.setKoty(ile);
	cout << "Ile masz psow? " << endl;
	wejscie >> ile;
	w.setPsy(ile);

	cout << "Masz razem " << w.getKoty() + w.getPapugi() + w.getPsy() << " zwierzat." << endl;
	w.ustaw();
	return wejscie;
}
void wlasciciel::wyswietl() {
	for (int i = 0; i < getKoty() + getPapugi() + getPsy(); i++) {
	gromada[i]->jedz();
	}
}
