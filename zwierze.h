#include <iostream>
#include <string>
using namespace std;
class zwierze {
private:
	int wiek;
	string nazwa;
protected:
	static int ile_zwierzat;
public:
	string getName() const;
	void setName(string name = "Zwierze");
	int getAge() const;
	void setAge(int age = 0);
	virtual void jedz() = 0;
	virtual void dajG�os();
	friend ostream & operator<<(ostream &wyjscie, const zwierze &z);
	friend istream & operator>>(istream &wejscie, zwierze &z);
	static void ileZwierzat() { 
		cout << "Jest " << ile_zwierzat << endl;
	}
	virtual ~zwierze();
};
class zwierzeDomowe {
public:
	virtual void przytul(zwierze *z);
	virtual ~zwierzeDomowe() {
		cout << "Zwierze domowe zdechlo." << endl;
	}
};
class pies : public zwierze, public zwierzeDomowe {
public:
	pies(int wiek = 0, string nazwa = "Pies") {
		setName(nazwa);
		setAge(wiek);
		ile_zwierzat++;
	}
	void jedz();
	void dajG�os();
	void przytul(zwierze *z);
	~pies() {
		cout << "Pies zdechl." << endl;
		ile_zwierzat--;
	}
};
class kot : public zwierze, public zwierzeDomowe {
public:
	kot(int wiek = 0, string nazwa = "Kot") {
		setName(nazwa);
		setAge(wiek);
		ile_zwierzat++;
	}
	void jedz();
	void dajG�os();
	void przytul(zwierze *z);
	~kot() {
		cout << "Kot zdechl." << endl;
		ile_zwierzat--;
	}
};
class ptak : public zwierze {
public:
	void lataj();
	virtual void jedz() = 0;
	virtual ~ptak() {
		cout << "Ptak zdechl." << endl;
	}
};
class papuga : public ptak {
public:
	papuga(int wiek = 0, string nazwa = "Papuga") {
		setName(nazwa);
		setAge(wiek);
		ile_zwierzat++;
	}
	void jedz();
	void dajG�os();
	~papuga() {
		cout << "Papuga zdechla." << endl;
		ile_zwierzat--;
	}
};
class wlasciciel {
private:
	string imie;
	int papugi = 0;
	int koty = 0;
	int psy = 0;
	zwierze** gromada;
	void ustaw();
public:
	void setPapugi(int ile = 0);
	void setPsy(int ile = 0);
	void setKoty(int ile = 0);
	void setImie(string name = "");
	string getImie() { return imie; }
	int getPapugi() { return papugi; }
	int getPsy() { return psy; }
	int getKoty() { return koty; }
	wlasciciel(string imie, int papugi, int psy, int koty) {
		this->imie = imie;
		this->papugi = papugi;
		this->psy = psy;
		this->koty = koty;
		ustaw();
	}
	~wlasciciel() {
		cout << getImie() << " umarl." << endl;
	}
	void wyswietl();
	friend istream & operator>>(istream &wejscie, wlasciciel &w);
	
};
